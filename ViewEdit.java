import java.util.*;

public class ViewEdit
{
	public ViewEdit(){}
	
	public void run()
	{
		ArrayList<Student> studentList = new ArrayList<Student>();

		Student s1 = new Student("Tan Li May", 1, "yes\t\tno\t\tyes");
		Student s2 = new Student("Tan Ren Yi", 2, "no\t\tyes\t\tno");
		Student s3 = new Student("Diane Lim", 3, "mc\t\tyes\t\tno");
		Student s4 = new Student("Chua Qi Yang", 4, "yes\t\tmc\t\tno");
		Student s5 = new Student("Khairunnisa", 5, "no\t\tyes\t\tmc");
		
		String studUpdate, seshup, sessionChange;


		studentList.add(s1);
		studentList.add(s2);
		studentList.add(s3);
		studentList.add(s4);
		studentList.add(s5);
		 
		Scanner scn = new Scanner(System.in);
		System.out.println("Please enter which course attendance you would like to view and edit\n1)PSD3\n2)TP3\n3)ALG3\n4)Exit\n");
		int sess = scn.nextInt();

		System.out.println("No\tName\t\tIndex No.\tSession 1\tSession 2\tSession 3");
		for(int i = 0; i<studentList.size(); i++)
		{
			Student s = (Student) studentList.get(i);
			System.out.println((i+1) + "\t" + s.getName() + "\t\t" + s.getIndexNo() + "\t" + s.getAttendance());
		}


		System.out.println("Please key which index student attendance to edit");
		studUpdate = scn.next();
		System.out.println("You have chosen to update " + studUpdate);

		System.out.println("Please enter the session to edit");
		seshup = scn.next();

		System.out.println("You have chosen to update session " + seshup);

		System.out.println("Please enter the update for the session");
		sessionChange = scn.next();

		System.out.println("You have updated");

		scn.close();
	}
}
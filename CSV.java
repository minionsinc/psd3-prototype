import java.io.FileWriter;
import java.io.IOException;

public class CSV
{
	private FileWriter writer;
	
	public CSV(String filename, String[] headers)
	{
		try
		{
			writer = new FileWriter(filename);
			addRow(headers);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void addRow(String[] row)
	{
		try
		{
			boolean first = true;
			for(String data : row)
			{
				if(!first)
					writer.append(',');
				else
					first = false;
				writer.append(data);
			}
			writer.append('\n');
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public void generateCSV()
	{
		try
		{
			writer.flush();
			writer.close();
		}
		catch(IOException e)
		{
			 e.printStackTrace();
		} 
	}
}
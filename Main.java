import java.util.Scanner;

public class Main{

	public static void main(String[] args) {
		for (int i = 0; i < 50; ++i) System.out.println();
		Scanner userInputScanner = new Scanner(System.in);
		System.out.println("Select your account type from the following ");
		System.out.println("1. Administrator");
		System.out.println("2. Tutor");
		System.out.println("Please enter your selection : ");
		int usertype = userInputScanner.nextInt();
		String username;
		String password;
		int selection;
		switch(usertype)
		{
		case 1 : 
			for (int i = 0; i < 50; ++i) System.out.println();
			System.out.println("Please key in your username.");
			username = userInputScanner.next();
			for (int i = 0; i < 50; ++i) System.out.println();
			if(username!=null)
			{
				System.out.println("Please key in your password.");
				password = userInputScanner.next();
				for (int i = 0; i < 50; ++i) System.out.println();
				if(password!=null)
				{
					System.out.println("Welcome to the system, administrator");
					System.out.println("Do you want to : ");
					System.out.println("1. Export Grades");
					selection = userInputScanner.nextInt();
					for (int i = 0; i < 50; ++i) System.out.println();
					while(selection ==1)
					{

						System.out.println("Do you want to : ");
						System.out.println("1. Export By Course");
						System.out.println("2. Export by Student");
						selection = userInputScanner.nextInt();
						for (int i = 0; i < 50; ++i) System.out.println();

						switch(selection)
						{
							case 1:
								ByCourse bc = new ByCourse();
								bc.run();
								break;
							case 2:
								view_student vs = new view_student();
								vs.run();
						}
						break;
					}
				}
			}
			break;
		case 2 :
			for (int i = 0; i < 50; ++i) System.out.println();
			System.out.println("Please key in your username.");
			username = userInputScanner.next();
			if(username!=null){
				for (int i = 0; i < 50; ++i) System.out.println();
				System.out.println("Please key in your password.");
				password = userInputScanner.next();
				if(password!=null){
					
					for (int i = 0; i < 50; ++i) System.out.println();
					System.out.println("Action: ");
					System.out.println("1. View list of students");
					System.out.println("2. Mark Attendance");
					System.out.println("3. Upload Attendance from Barcode Scanner");
					System.out.println("4. View/Edit Attendance");
					System.out.println("Please enter your selection : ");
					selection = userInputScanner.nextInt();
					for (int i = 0; i < 50; ++i) System.out.println();
					switch(selection)
					{
						case 1:
							session_view_student svs = new session_view_student();
							svs.run();
							break;
						case 2:
							MarkAttendance ma = new MarkAttendance();
							ma.run();
							break;
						case 3:
							userInputScanner.nextLine();
							System.out.println("Please enter input file name : ");
							String csvFile = userInputScanner.nextLine();
							ReadCSV rc = new ReadCSV();
							rc.run(csvFile);
							break;
						case 4:
							ViewEdit ve = new ViewEdit();
							ve.run();
							break;
					}
					
					break;
				}
			}
		default :
			break;
		}


	}
}

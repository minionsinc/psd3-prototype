import java.util.*;

	public class ByCourse
	{
		public ByCourse()
		{
		}
		public void run()
		{
			CSV csv;
			Scanner scn = new Scanner(System.in);       
        
            displayMenu();
         	
			int option = scn.nextInt();
			for (int i = 0; i < 50; ++i) System.out.println();
			String modDetails = "";
			String[] results = new String[2];
		
			switch (option){
			case 1: 
					results[0] = "Name";
					results[1] = "Results";
					csv = new CSV("PSD3 Results.csv", results);
					modDetails = "Data for module PSD3:-\n Qiyang: A\n LiMay: A\n Nisa: B\n Diane: A\n RenYi: B\n";
					results[0] = "Qiyang";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "LiMay";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "Nisa";
					results[1] = "B";
					csv.addRow(results);
					results[0] = "Diane";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "RenYi";
					results[1] = "B";
					csv.addRow(results);
					csv.generateCSV();
					break;
			case 2: 
					results[0] = "Name";
					results[1] = "Results";
					csv = new CSV("TP3 Results.csv", results);
					modDetails = "Data for module TP3:-\n Qiyang: A\n LiMay: B\n Nisa: A\n Diane: A\n RenYi: B\n";
					results[0] = "Qiyang";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "LiMay";
					results[1] = "B";
					csv.addRow(results);
					results[0] = "Nisa";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "Diane";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "RenYi";
					results[1] = "B";
					csv.addRow(results);
					csv.generateCSV();
					break;
			case 3: 
					results[0] = "Name";
					results[1] = "Results";
					csv = new CSV("ALG3 Results.csv", results);
					modDetails = "Data for module ALG3:-\n Qiyang: B\n LiMay: A\n Nisa: B\n Diane: A\n RenYi: B\n";
					results[0] = "Qiyang";
					results[1] = "B";
					csv.addRow(results);
					results[0] = "LiMay";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "Nisa";
					results[1] = "B";
					csv.addRow(results);
					results[0] = "Diane";
					results[1] = "A";
					csv.addRow(results);
					results[0] = "RenYi";
					results[1] = "B";
					csv.addRow(results);
					csv.generateCSV();
					break;
			}
            System.out.println(modDetails);
         
        }		
      
   	
       public static void displayMenu() 
      {
         System.out.println();
         System.out.println("-------------- List of Modules --------------");
         System.out.println("[1] PSD3 - Professional Software Development 3");
         System.out.println("[2] TP3 - Team  Project 3");
         System.out.println("[3] ALG3 - Algorithmics");
         System.out.println("[4] Exit");		
         System.out.println("------------------------------------------");
         System.out.print("Enter your option : ");		
      }
	}
			
			
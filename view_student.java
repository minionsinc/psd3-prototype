import java.util.Scanner;

public class view_student{

	public view_student()
	{
	}
	
	public void run() {
		Scanner input = new Scanner(System.in);
		System.out.println("Please Select Student name: \n1.QiYang \n2.LiMay \n3.Nisa \n4.Diane \n5.RenYi \n6.Exit");
		System.out.println("Enter Your Choice: ");
		CSV csv;
		String[] results = new String[2];
		
		int student = input.nextInt();
		for (int i = 0; i < 50; ++i) System.out.println();
	
		switch(student){
		case 1:				
			System.out.println("Courses Taken and Grades of QiYang\n -TP3 : A \n -ALG3: B \n -PSD3: A \n");
			results[0] = "Subject";
			results[1] = "Results";
			csv = new CSV("QiYang Results.csv", results);
			results[0] = "TP3";
			results[1] = "A";
			csv.addRow(results);
			results[0] = "ALG3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "PSD3";
			results[1] = "A";
			csv.addRow(results);
			csv.generateCSV();
			break; 
		case 2:
			System.out.println("Courses Taken and Grades of LiMay\n -TP3 : B \n -ALG3: A \n -PSD3: A \n");
			results[0] = "Subject";
			results[1] = "Results";
			csv = new CSV("LiMay Results.csv", results);
			results[0] = "TP3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "ALG3";
			results[1] = "A";
			csv.addRow(results);
			results[0] = "PSD3";
			results[1] = "A";
			csv.addRow(results);
			csv.generateCSV();
			break;			
		case 3:
			System.out.println("Courses Taken and Grades of Nisa\n -TP3 : A \n -ALG3: B \n -PSD3: B \n");
			results[0] = "Subject";
			results[1] = "Results";
			csv = new CSV("Nisa Results.csv", results);
			results[0] = "TP3";
			results[1] = "A";
			csv.addRow(results);
			results[0] = "ALG3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "PSD3";
			results[1] = "B";
			csv.addRow(results);
			csv.generateCSV();
			break;	
		case 4:
			System.out.println("Courses Taken and Grades of Diane\n -TP3 : A \n -ALG3: B \n -PSD3: A \n");
			results[0] = "Subject";
			results[1] = "Results";
			csv = new CSV("Diane Results.csv", results);
			results[0] = "TP3";
			results[1] = "A";
			csv.addRow(results);
			results[0] = "ALG3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "PSD3";
			results[1] = "A";
			csv.addRow(results);
			csv.generateCSV();
			break;	
		case 5:
			System.out.println("Courses Taken and Grades of RenYi\n -TP3 : B \n -ALG3: B \n -PSD3: B \n");
			results[0] = "Subject";
			results[1] = "Results";
			csv = new CSV("RenYi Results.csv", results);
			results[0] = "TP3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "ALG3";
			results[1] = "B";
			csv.addRow(results);
			results[0] = "PSD3";
			results[1] = "B";
			csv.addRow(results);
			csv.generateCSV();
			break;			
		default:
			System.out.println("You Choose to Exit. Thank You!"); 
			break;
		}
	}
}	

import java.util.*;

public class Student
{
	private String name;
	private int indexNo;
	private String attendanceR;

	public Student()
	{}

	public Student(String n, int i, String a)
	{
		name = n;
		indexNo = i;
		attendanceR = a;
	}

	public void setName(String n)
	{
		name = n;
	}

	public String getName()
	{
		return name;
	}

	public void setIndexNo(int i)
	{
		indexNo = i;
	}

	public int getIndexNo()
	{
		return indexNo;
	}

	public void setAttendance(String a)
	{
		attendanceR = a;
	}

	public String getAttendance()
	{
		return attendanceR;
	}
}